Mixin library
===

Library to ease the use of mixins in JS.
You can see how the library works checking the tests we added.

## Methods provided


  * [mixOf(target, mixin)](#module_mixOf)
  * [create(name, mixinProperties)](#module_create) ⇒ <code>Object</code>

<a name="module_mixOf"></a>

### mixOf(target, mixin)
Merges the attributes of mixin to the prototype of target.

| Param | Type | Description |
| --- | --- | --- |
| target | <code>function</code> | Constructor of the class that will be merged with the mixins |
| mixin | <code>Object</code> &#124; <code>Array</code> | Object (or array of objects) that contains the attributes and methods to be merged with target. |

<a name="module_create"></a>

### create(name, mixinProperties) ⇒ <code>Object</code>
Creates a Mixin object with the name and methods passed as parameters.

| Param | Type | Description |
| --- | --- | --- |
| name | <code>String</code> | Name of the Mixin. |
| mixinProperties | <code>Object</code> | Properties to be added to the Mixin |
