/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var assert  = require('assert');
var MixinManager = require ('../');

describe('MixinManager', function() {

    var mixinList;

    before(function() {
    	mixinList = [
    		MixinManager.create('Mixin1', {
    			moveUp: function(){
				    return "move up";
				  }
    		}),
    		MixinManager.create('Mixin2', {
    			moveDown: function() {
				    return "move down " + this.testValue;
				  }
    		}),
    		MixinManager.create('Mixin3', {
    			stop: function() {
				    return "stop";
				  }
    		})
    	];
    });

    describe('When merging a mixin', function() {

      it('should add the method of the mixin', function() {

      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

      	MixinManager.mixOf(ParentClass, mixinList[0]);

      	var parentInstance = new ParentClass();

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.moveUp(), 'move up');
      });

      it('should not override a method of the Target Class', function() {

      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

	    	ParentClass.prototype.moveUp = function() {
	    		return "moveUp";
	    	};

      	MixinManager.mixOf(ParentClass, mixinList[0]);

      	var parentInstance = new ParentClass();

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.moveUp(), 'moveUp');
      });

      it('should add the method of multiple mixins', function() {

      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

      	MixinManager.mixOf(ParentClass, mixinList[0]);
      	MixinManager.mixOf(ParentClass, mixinList[2]);

      	var parentInstance = new ParentClass();

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.moveUp(), 'move up');
      	assert.equal(parentInstance.stop(), 'stop');
      });

      it('should add all the methods of a list of mixins', function() {

      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

      	var localMixinList = [mixinList[0], mixinList[2]];
      	MixinManager.mixOf(ParentClass, localMixinList);

      	var parentInstance = new ParentClass();

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.moveUp(), 'move up');
      	assert.equal(parentInstance.stop(), 'stop');
      });

      it('should add a method to an instance', function() {
      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

      	MixinManager.mixOf(ParentClass, mixinList[0]);

      	var parentInstance = new ParentClass();

      	MixinManager.mixOf(ParentClass, mixinList[2]);

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.moveUp(), 'move up');
      	assert.equal(parentInstance.stop(), 'stop');
      });

      it('should be accessible using the mixin name', function() {
        var ParentClass = function() {};
        ParentClass.prototype.move = function() {
          return "move";
        };

        MixinManager.mixOf(ParentClass, mixinList[0]);

        var parentInstance = new ParentClass();

        MixinManager.mixOf(ParentClass, mixinList[2]);

        assert.equal(parentInstance.move(), 'move');
        assert.equal(parentInstance.moveUp(), 'move up');
        assert.equal(parentInstance.stop(), 'stop');

        assert.equal(parentInstance.__super['Mixin3'].stop(), 'stop');
      });

      it('should fail if a method is overwritten twice', function() {

      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

        MixinManager.mixOf(ParentClass, mixinList[0]);
        MixinManager.mixOf(ParentClass, mixinList[0]);

      	var parentInstance = new ParentClass();
      	assert.throws(function(){parentInstance.moveUp()}, Error);
      });

      it('should not add a property from a mixin', function() {
      	var ParentClass = function() {};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

      	MixinManager.mixOf(ParentClass, mixinList[1]);

      	var parentInstance = new ParentClass();

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.testValue, undefined);
      });

      it('should access the object state', function() {
      	var ParentClass = function() {
      		this.testValue = 'Test';
      	};
	    	ParentClass.prototype.move = function() {
	    		return "move";
	    	};

      	MixinManager.mixOf(ParentClass, mixinList[1]);

      	var parentInstance = new ParentClass();

      	assert.equal(parentInstance.move(), 'move');
      	assert.equal(parentInstance.moveDown(), 'move down Test');
      	assert.equal(parentInstance.testValue, 'Test');
      });
    });
});