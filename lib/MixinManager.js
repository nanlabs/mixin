/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');

/**
 * Provides a method to merge different mixins to a class.
 * @module MixinManager
 */

module.exports = {

	superPropertyKey: '__super',

	isMixinFunctionProperty: 'isMixin',

	/**
	 * Checks whether a function is part of a mixin
	 * @param  {Function}  functionObject Function object to be evaluated.
	 * @param  {String}  functionName   Name of the function to be evaluated.
	 * @return {Boolean}                
	 */
	isMixinFunctionPredicate: function(functionObject, functionName) {
		return !!functionObject[this.isMixinFunctionProperty];
	},

	/**
	 * Builds a function that throws an error showing a conflict between a list of mixins.
	 * @param  {String} duplicatedKey     Name of the function that will be created.
	 * @param  {Array} conflictingMixins List of mixins that are in conflict.
	 * @return {Function}                   The function that returns the conflicting error.
	 */
	buildMixinConflictFunction: function(duplicatedKey, conflictingMixins) {
		var conflictFunction = function() {
			throw new Error('Name conflict during method ' + duplicatedKey + ' execution. This method can be found in the following mixins: ' + conflictingMixins);
		};

		conflictFunction[this.isMixinFunctionProperty] = true;
		return conflictFunction;
	},

	/**
	 * Returns a list of Mixin objects that have defined a certain function (passed in the functionName parameter).
	 * @param  {String} functionName Name of the function to be used as predicate of the search.
	 * @param  {Function} target       Class that contains the mixins.
	 * @return {Array}              List of mixins that contain the function name functionName.
	 */
	findMixinsByFunctionName: function(functionName, target) {
		var parentList = target.prototype[this.superPropertyKey];
		return _.filter(parentList, function(parentItem) {
			return !!parentItem.properties[functionName];
		});		
	},

	/**
	 * Builds a function that wraps a mixin function, selecting the proper mixin.
	 * @param  {Function} target       Class where the function will be defined.
	 * @param  {String} functionName Name of the function the will be wrapped.
	 * @param  {String} mixinName    Name of the mixin that contains the definition of the function.
	 * @return {Function}              Function object that wraps the mixin function.
	 */
	buildWrapperMixinFunction: function(target, functionName, mixinName) {
		var mixinWrapper = target.prototype[this.superPropertyKey][mixinName].properties[functionName];
		mixinWrapper[this.isMixinFunctionProperty] = true;
		return mixinWrapper;
	},

	/**
	 * Builds a shortcut function to access the mixin function directly (avoiding the properties attribute).
	 * @param  {Object} mixin        Mixin object.
	 * @param  {String} functionName Name of the function the will be wrapped.
	 */
	buildShortcutFunctionForMixin: function(mixin, functionName) {
		mixin[functionName] = mixin.properties[functionName];
	},

	/**
	 * Merges the attributes of mixin to the prototype of target.
	 * @param  {Function} target Constructor of the class that will be merged with the mixins
	 * @param  {Object|Array} mixin Object (or array of objects) that contains the attributes and methods to be merged with target.
	 */
  mixOf: function(target, mixin) {
  	var oldConstructor = target.constructor;

  	var newConstructor = function() {
  		_(this.prototype)
  		.pick(function(functionObject, functionName) {
				return !!functionObject.isMixin;
			}).forEach(function(functionObject, functionName) {
				this.prototype[functionName] = functionObject.bind(this);
			});

  		oldConstructor.apply(this, arguments);
  	};

  	target.constructor = newConstructor;

  	var mixins;
  	if (_.isArray(mixin)) {
  		mixins = mixin;
  	} else {
  		mixins = [mixin];
  	}

  	if (!target.prototype[this.superPropertyKey]) {
  		target.prototype[this.superPropertyKey] = {};
  	}

		// workaround for it to work on ES6 and older versions
		var targetKeys;
		if(Object.getOwnPropertyNames){
			targetKeys = Object.getOwnPropertyNames(target.prototype);
		}else{
			targetKeys = _.keys(target.prototype);
		}
		var originalTargetKeys = _.filter(targetKeys, function(key){
			return !this.isMixinFunctionPredicate(target.prototype[key]) &&
				key !== this.superPropertyKey;
		}.bind(this));

  	var mixedKeys = _.difference(targetKeys, originalTargetKeys);
  	_.forEach(mixins, function(mixinItem) {

	    if (mixinItem.properties.__init) {
	      mixinItem.properties.__init.bind(mixinItem)();
	    }

  		var mixinKeys = _.keys(mixinItem.properties);
  		var duplicatedKeys = _.intersection(mixedKeys, mixinKeys);

  		if (!_.isEmpty(duplicatedKeys)) {
  			_.forEach(duplicatedKeys, function(duplicatedKey) {
  				var originalMixins = this.findMixinsByFunctionName(duplicatedKey, target);
  				var mixinsNames = _.map(originalMixins, 'name');
  				mixinsNames.push(mixinItem.name);
  				target.prototype[duplicatedKey] = this.buildMixinConflictFunction(duplicatedKey, mixinsNames);
  			}.bind(this));
  		}

	    target.prototype[this.superPropertyKey][mixinItem.name] = mixinItem;

	    _.forEach(mixinKeys, function(mixinKey) {
	    	if (_.isFunction(mixinItem.properties[mixinKey]) && 
	    		!_.includes(originalTargetKeys, mixinKey) && 
	    		!_.includes(duplicatedKeys, mixinKey)) {
	    		target.prototype[mixinKey] = this.buildWrapperMixinFunction(target, mixinKey, mixinItem.name);
	    	}
	    }.bind(this));
	  }.bind(this));
  },

  /**
   * Creates a Mixin object with the name and methods passed as parameters.
   * @param  {String} name            Name of the Mixin.
   * @param  {Object} mixinProperties Properties to be added to the Mixin
   * @return {Object}                 
   */
  create: function(name, mixinProperties) {
  	var mixinObject = {
  		name: name,
  		properties: mixinProperties
  	};

  	_.forEach(mixinProperties, function(functionObject, functionName) {
  		this.buildShortcutFunctionForMixin(mixinObject, functionName);
  	}.bind(this));

  	return mixinObject;
  }
};